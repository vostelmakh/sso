# syntax=docker/dockerfile:1.4
FROM --platform=$BUILDPLATFORM golang:1.21-alpine AS builder

WORKDIR /code

ENV CGO_ENABLED 1
ENV GOPATH /go
ENV GOCACHE /go-build

RUN apk add gcc musl-dev

COPY go.mod go.sum ./

COPY .env ./

RUN --mount=type=cache,target=/go/pkg/mod/cache \
    go mod download

COPY . .

RUN --mount=type=cache,target=/go/pkg/mod/cache \
    --mount=type=cache,target=/go-build \
    go build -o bin/backend cmd/sso/main.go

CMD ["/code/bin/backend"]

RUN mkdir -p ./migrations && \
    mkdir -p ./db && \
    touch db/sso.db

RUN --mount=type=cache,target=/go/pkg/mod/cache \
    --mount=type=cache,target=/go-build \
    go build -o bin/migrate cmd/migrate/main.go

RUN /code/bin/migrate

FROM builder as dev-envs

RUN <<EOF
apk update
apk add git
EOF

RUN <<EOF
addgroup -S docker
adduser -S --shell /bin/bash --ingroup docker vscode
EOF

COPY --from=gloursdocker/docker / /

EXPOSE 44044

CMD ["go", "run", "main.go"]

FROM scratch
COPY --from=builder /code/bin/backend /usr/local/bin/backend
CMD ["/usr/local/bin/backend"]
