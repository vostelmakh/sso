package main

import (
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/vostelmakh/sso/internal/app"
	loggerFactory "gitlab.com/vostelmakh/sso/internal/app/factory"
	"gitlab.com/vostelmakh/sso/internal/config"
)

func main() {
	config := config.MustLoad()

	logger := loggerFactory.NewLogger()

	logger.Info("Start server: ", time.Now().String())
	logger.Info("ENV:", config.Env)
	logger.Info("STORAGE_PATH:", config.StoragePath)

	application := app.New(logger, config.GrpcConfig.Port, config.HttpPort, config.StoragePath, config.TokenTtl)

	go application.GRPCServer.Run()
	go application.HTTPServer.Run()

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, syscall.SIGTERM, syscall.SIGINT)

	<-stop

	application.GRPCServer.Stop()
	application.HTTPServer.Stop()

	logger.Info("App stopped")
}
