package main

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"os"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/sqlite3"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		fmt.Printf("err loading: %v", err)
	}

	storagePath := os.Getenv("STORAGE_PATH")
	table := os.Getenv("MIGRATION_TABLE")

	databaseUrl := fmt.Sprintf("sqlite3://%s?x-migrations-table=%s", storagePath, table)
	sourceUrl := os.Getenv("MIGRATION_SOURCE_URL")

	db, err := sql.Open("sqlite3", storagePath)
	if err != nil {
		log.Fatalf("Failed to open SQLite database: %v", err)
	}

	driver, err := sqlite3.WithInstance(db, &sqlite3.Config{})
	if err != nil {
		log.Fatalf("Failed to create SQLite driver: %v", err)
	}

	migrator, err := migrate.NewWithDatabaseInstance(
		sourceUrl,
		databaseUrl,
		driver,
	)

	if err != nil {
		panic(err)
	}

	if err := migrator.Up(); err != nil {
		if errors.Is(err, migrate.ErrNoChange) {
			fmt.Println("no migrations to apply")

			return
		}

		panic(err)
	}

	fmt.Println("Migrations were applied")
}
