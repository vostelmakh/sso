package auth

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/vostelmakh/sso/internal/services/auth"
)

type handler struct {
	auth Auth
}

type Auth interface {
	Login(
		ctx context.Context,
		email string,
		password string,
		appID int,
	) (token string, err error)
	Register(
		ctx context.Context,
		email string,
		password string,
	) (int64, error)
	Verify(
		ctx context.Context,
		token string,
		appID int,
	) (string, error)
}

func NewHandler(auth Auth) http.Handler {
	return &handler{auth: auth}
}

func (h *handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	handlers := map[string]func(http.ResponseWriter, *http.Request){
		"/":         h.handleRoot,
		"/login":    h.handleLogin,
		"/register": h.handleRegister,
		"/verify":   h.handleVerifyToken,
	}

	handler, ok := handlers[r.URL.Path]
	if !ok {
		http.NotFound(w, r)
		return
	}

	handler(w, r)
}

func (h *handler) handleLogin(w http.ResponseWriter, r *http.Request) {
	var loginReq loginRequest
	err := json.NewDecoder(r.Body).Decode(&loginReq)
	if err != nil {
		http.Error(w, "Invalid request body", http.StatusBadRequest)
		return
	}

	if loginReq.Email == "" || loginReq.Password == "" || loginReq.AppID == 0 {
		http.Error(w, "Missing required fields", http.StatusBadRequest)
		return
	}

	token, err := h.auth.Login(r.Context(), loginReq.Email, loginReq.Password, loginReq.AppID)
	if err != nil {
		if errors.Is(err, auth.ErrInvalidCredentials) {
			http.Error(w, "Invalid email or password", http.StatusUnauthorized)
			return
		} else {
			http.Error(w, "Internal server error: "+err.Error(), http.StatusInternalServerError)
			return
		}
	}

	w.Header().Set("Authorization", "Bearer "+token)

	response := loginResponse{Message: "Login successful"}
	err = json.NewEncoder(w).Encode(response)
	if err != nil {
		http.Error(w, "Error encoding response", http.StatusInternalServerError)
		return
	}
}

type loginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
	AppID    int    `json:"appId"`
}

type loginResponse struct {
	Message string `json:"token"`
}

func (h *handler) handleRegister(w http.ResponseWriter, r *http.Request) {
	var registerReq registerRequest
	err := json.NewDecoder(r.Body).Decode(&registerReq)
	if err != nil {
		http.Error(w, "Invalid request body", http.StatusBadRequest)
		return
	}

	if registerReq.Email == "" || registerReq.Password == "" {
		http.Error(w, "Missing required fields", http.StatusBadRequest)
		return
	}

	userId, err := h.auth.Register(r.Context(), registerReq.Email, registerReq.Password)
	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	response := registerResponse{UserId: userId}
	err = json.NewEncoder(w).Encode(response)
	if err != nil {
		http.Error(w, "Error encoding response", http.StatusInternalServerError)
		return
	}
}

type registerRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type registerResponse struct {
	UserId int64 `json:"userId"`
}

func (h *handler) handleRoot(writer http.ResponseWriter, _ *http.Request) {
	_, err := fmt.Fprintf(
		writer, `
				  ##         .
			## ## ##        ==
		 ## ## ## ## ##    ===
		/"""""""""""""""""\___/ ===
		{                       /  ===-
		\______ O           __/
		 \    \         __/
		  \____\_______/
			
		Hello from Docker!
		`,
	)

	if err != nil {
		http.Error(writer, "Missing userId parameter", http.StatusBadRequest)
		return
	}
}

func (h *handler) handleVerifyToken(w http.ResponseWriter, r *http.Request) {
	authHeader := r.Header.Get("Authorization")
	if authHeader == "" {
		http.Error(w, "Authorization header is missing", http.StatusUnauthorized)
		return
	}

	parts := strings.Split(authHeader, " ")
	if len(parts) != 2 || parts[0] != "Bearer" {
		http.Error(w, "Invalid Authorization header format", http.StatusUnauthorized)
		return
	}

	tokenString := parts[1]

	var verifyRequest verifyRequest
	err := json.NewDecoder(r.Body).Decode(&verifyRequest)
	if err != nil {
		http.Error(w, "Invalid request body", http.StatusBadRequest)
		return
	}

	_, err = h.auth.Verify(r.Context(), tokenString, verifyRequest.AppID)
	if err != nil {
		http.Error(w, "Invalid token: "+err.Error(), http.StatusUnauthorized)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Token is OK"))
}

type verifyRequest struct {
	AppID int `json:"AppID"`
}
