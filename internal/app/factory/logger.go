package factory

import (
	"log/slog"
	"os"
)

const (
	envLocal = "LOCAL"
	envDev   = "DEV"
	envProd  = "PROD"
)

func NewLogger() *slog.Logger {
	var logger *slog.Logger

	env := os.Getenv("APP_ENV")

	switch env {
	case envLocal:
		logger = slog.New(
			slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}),
		)
	case envDev:
		logger = slog.New(
			slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}),
		)
	case envProd:
		logger = slog.New(
			slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelInfo}),
		)
	}

	return logger
}
