package http

import (
	"context"
	"fmt"
	"log/slog"
	_ "net"
	"net/http"
	"time"

	authhttp "gitlab.com/vostelmakh/sso/internal/http/auth"
)

type App struct {
	logger     *slog.Logger
	httpServer *http.Server
	port       int
}

func New(
	logger *slog.Logger,
	authService authhttp.Auth,
	port int,
) *App {
	httpServer := &http.Server{Addr: fmt.Sprintf(":%d", port)}

	httpServer.Handler = authhttp.NewHandler(authService)

	return &App{
		logger:     logger,
		httpServer: httpServer,
		port:       port,
	}
}

func (a *App) Run() error {
	const op = "http.app.Run"

	logger := a.logger.With(
		slog.String("op", op),
		slog.Int("port", a.port),
	)

	logger.Info("HTTP server is running", slog.String("addr", a.httpServer.Addr))

	return a.httpServer.ListenAndServe()
}

func (a *App) Stop() {
	const op = "http.app.Stop"

	logger := a.logger.With(
		slog.String("op", op),
		slog.Int("port", a.port),
	)

	logger.Info("Stop HTTP server")

	shutdownCtx, shutdownRelease := context.WithTimeout(context.Background(), 10*time.Second)
	defer shutdownRelease()

	if err := a.httpServer.Shutdown(shutdownCtx); err != nil {
		logger.Error("HTTP shutdown error: %v", err)
	}

	logger.Info("Graceful shutdown complete.")
}
