package app

import (
	grpcApp "gitlab.com/vostelmakh/sso/internal/app/grpc"
	httpApp "gitlab.com/vostelmakh/sso/internal/app/http"
	"gitlab.com/vostelmakh/sso/internal/services/auth"
	sqlite "gitlab.com/vostelmakh/sso/internal/storage/sqllite"
	"log/slog"
	"time"
)

type App struct {
	GRPCServer *grpcApp.App
	HTTPServer *httpApp.App
}

func New(
	logger *slog.Logger,
	grpcPort int,
	httpPort int,
	storagePath string,
	tokenTtl time.Duration,
) *App {
	storage, err := sqlite.New(storagePath)
	if err != nil {
		panic(err)
	}

	authService := auth.New(logger, storage, storage, storage, tokenTtl)

	grpcApp := grpcApp.New(logger, authService, grpcPort)
	httpApp := httpApp.New(logger, authService, httpPort)

	return &App{
		GRPCServer: grpcApp,
		HTTPServer: httpApp,
	}
}
