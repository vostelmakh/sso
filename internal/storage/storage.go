package storage

import "errors"

var (
	ErrUserExists   = errors.New("user is not exist")
	ErrUserNotFound = errors.New("user was not found")
	ErrAppNotFound  = errors.New("app was not found")
)
